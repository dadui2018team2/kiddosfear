/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ACTIVATIONOFPAINT = 47363338U;
        static const AkUniqueID BALLFALLING = 3893334395U;
        static const AkUniqueID BALLNOAIR = 1006844457U;
        static const AkUniqueID BALLSPINNING = 3877046500U;
        static const AkUniqueID BOSSSOUND = 862239991U;
        static const AkUniqueID BOSSSPAWNING = 2472930969U;
        static const AkUniqueID COLORPICKUPS = 407893481U;
        static const AkUniqueID DOORISLOCKEDUP = 847537072U;
        static const AkUniqueID ENDLEVELFEEDBACK = 2368197235U;
        static const AkUniqueID GREENCOLOR = 1273621539U;
        static const AkUniqueID KEYUI = 3227864640U;
        static const AkUniqueID KIDDOFOOTSTEPS = 2266394309U;
        static const AkUniqueID KIDDOLANDING = 3128530969U;
        static const AkUniqueID MONSTERSOUND = 512170010U;
        static const AkUniqueID PICKUPLIGHT = 2491260467U;
        static const AkUniqueID PLAY_INTRO = 3103472528U;
        static const AkUniqueID PLAY_MUSIC = 2932040671U;
        static const AkUniqueID PLAY_OUTRO = 431014199U;
        static const AkUniqueID PLAY_PAINT = 4010919902U;
        static const AkUniqueID PLAY_WATER = 441572235U;
        static const AkUniqueID PLAY_WHALE = 4074383009U;
        static const AkUniqueID STATUE = 939730917U;
        static const AkUniqueID STOP_GREENCOLOR = 1859929150U;
        static const AkUniqueID STOP_INTRO = 1837763790U;
        static const AkUniqueID STOP_MUSIC = 2837384057U;
        static const AkUniqueID STOP_OUTRO = 1027445333U;
        static const AkUniqueID STOP_PAINT = 2941236940U;
        static const AkUniqueID STOP_WATER = 86541213U;
        static const AkUniqueID STOP_WHALE = 333311239U;
        static const AkUniqueID TELEDOOR = 2999876861U;
        static const AkUniqueID UI = 1551306167U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MUSIC_KEY
        {
            static const AkUniqueID GROUP = 2078097994U;

            namespace STATE
            {
                static const AkUniqueID NOKEY = 470023953U;
                static const AkUniqueID ONEKEY = 1649261132U;
                static const AkUniqueID THREEKEYS = 3314977319U;
                static const AkUniqueID TWOKEYS = 1169404525U;
            } // namespace STATE
        } // namespace MUSIC_KEY

        namespace MUSIC_LEVEL
        {
            static const AkUniqueID GROUP = 2177735725U;

            namespace STATE
            {
                static const AkUniqueID LEVEL_1 = 1290008369U;
                static const AkUniqueID LEVEL_2 = 1290008370U;
                static const AkUniqueID LEVEL_3 = 1290008371U;
                static const AkUniqueID LEVEL_4 = 1290008372U;
                static const AkUniqueID MENU_HUB = 3087754956U;
            } // namespace STATE
        } // namespace MUSIC_LEVEL

        namespace SECRET
        {
            static const AkUniqueID GROUP = 3354192397U;

            namespace STATE
            {
                static const AkUniqueID SECRETFOUR = 1366044881U;
                static const AkUniqueID SECRETONE = 1792149047U;
                static const AkUniqueID SECRETTHREE = 3296666775U;
                static const AkUniqueID SECRETTWO = 2208191013U;
            } // namespace STATE
        } // namespace SECRET

        namespace STATEOFGAME
        {
            static const AkUniqueID GROUP = 2726287257U;

            namespace STATE
            {
                static const AkUniqueID INGAME = 984691642U;
                static const AkUniqueID MENU = 2607556080U;
            } // namespace STATE
        } // namespace STATEOFGAME

    } // namespace STATES

    namespace SWITCHES
    {
        namespace FOOTSTEPTYPE
        {
            static const AkUniqueID GROUP = 1458816175U;

            namespace SWITCH
            {
                static const AkUniqueID BLUE = 1325827433U;
                static const AkUniqueID NORMAL = 1160234136U;
            } // namespace SWITCH
        } // namespace FOOTSTEPTYPE

        namespace TELEDOOR
        {
            static const AkUniqueID GROUP = 2999876861U;

            namespace SWITCH
            {
                static const AkUniqueID TELEDOORAPPEAR = 2940176574U;
                static const AkUniqueID TELEDOORCLOSES = 144862798U;
                static const AkUniqueID TELEDOOROPEN = 1643113129U;
            } // namespace SWITCH
        } // namespace TELEDOOR

        namespace UI
        {
            static const AkUniqueID GROUP = 1551306167U;

            namespace SWITCH
            {
                static const AkUniqueID IUSTART = 3637873365U;
                static const AkUniqueID UISOUNDCLICK = 3194545524U;
                static const AkUniqueID UISOUNDSTARTSELECTLEVEL = 3944003874U;
            } // namespace SWITCH
        } // namespace UI

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID GREEN_RTCP = 1518550890U;
        static const AkUniqueID MASTER_VOLUME = 4179668880U;
        static const AkUniqueID MUSIC_VOLUME = 1006694123U;
        static const AkUniqueID SOUND_VOLUME = 495870151U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID ACTIVATIONOFPAINT = 47363338U;
        static const AkUniqueID ALLSOUNDS = 3278463154U;
        static const AkUniqueID ANIMATIONS = 2087567908U;
        static const AkUniqueID BOSSSOUND = 862239991U;
        static const AkUniqueID COLORPICKUPS = 407893481U;
        static const AkUniqueID ENDLEVELFEEDBACK = 2368197235U;
        static const AkUniqueID KIDDOFOOTSTEPS_01 = 2488681953U;
        static const AkUniqueID KIDDOLANDING = 3128530969U;
        static const AkUniqueID LOOPS = 225293542U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID PICKUPLIGHT_01 = 801015271U;
        static const AkUniqueID TELEDOOR = 2999876861U;
        static const AkUniqueID UISOUND_01 = 1423256018U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SOUND = 623086306U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
