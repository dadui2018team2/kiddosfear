﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropFloat : MonoBehaviour {

	[SerializeField] GameObject pool1;
    [SerializeField] GameObject pool2;
    [SerializeField] GameObject pool3;

    [SerializeField] float displacementMax;
    [SerializeField] float[] startingOffsets = new float[3];

    [SerializeField] float dampValue;
    [SerializeField] float maxSpeed;

    private void Start() {
        StartCoroutine(FloatAround(pool1, 0));
        StartCoroutine(FloatAround(pool2, 1));
        StartCoroutine(FloatAround(pool3, 2));
    }

    private IEnumerator FloatAround(GameObject pool, int startingOffsetIndex) {
        float currentOffset = startingOffsets[startingOffsetIndex];
        pool.transform.position = new Vector3(pool.transform.position.x, pool.transform.position.y + currentOffset, pool.transform.position.z);

        Vector3 goalPosition = pool.transform.position;

        Vector3 velocity = Vector3.zero;

        if (currentOffset < 0) {
            goalPosition.y -= displacementMax;
        }
        else
            goalPosition.y += displacementMax;

        while (true) {
            if (Vector3.Distance(pool.transform.position, goalPosition) > 0.1f) {
                pool.transform.position = Vector3.SmoothDamp(pool.transform.position, goalPosition, ref velocity, dampValue, maxSpeed);
            }
            else {
                goalPosition.y *= -1;
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
