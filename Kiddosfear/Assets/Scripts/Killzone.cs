﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killzone : MonoBehaviour {

	/*
	Killzone to call PlayerDeath in GameManager when the player enters its trigger. Only checks trigger as player uses CharacterController.
	 */

	private void Start() {
		GetComponent<BoxCollider>().isTrigger = true;
	}

	private void OnTriggerEnter(Collider other) {
		Check(other.gameObject.tag);
	}

	private void Check(string _tag){
		if (_tag == "Player"){
			GameManager.PlayerDeath();
		}
	}
}
