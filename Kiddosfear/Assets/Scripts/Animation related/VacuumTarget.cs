﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class VacuumTarget : MonoBehaviour {

	private Material material;
	private VacuumPoint vacuumPoint;

	private void Awake() {
		material = GetComponentInChildren<MeshRenderer>().material;
		if (!material) material = GetComponentInChildren<SkinnedMeshRenderer>().material;
		vacuumPoint = FindObjectOfType<VacuumPoint>();

		if (!vacuumPoint) this.enabled = false;

		material.SetFloat("_UseVacuum", 1f);
		Color c = Random.ColorHSV(0f, 1f, 0.5f, 1f, 0.9f, 1f, 1f, 1f);
		//c = new Color(Mathf.Sin(transform.position.x/4)/2 + 0.5f, Mathf.Sin(transform.position.y/4)/2 + 0.5f, Mathf.Cos(transform.position.x/4)/2 + 0.5f);
		
		material.SetColor("_Tint", c);
	}
	
	void Update () {
		material.SetFloat("_UseVacuum", vacuumPoint.vacuumOn);
		material.SetFloat("_VacuumStart", vacuumPoint.vacuumStart);
		material.SetFloat("_VacuumEnd", vacuumPoint.vacuumEnd);
		material.SetVector("_Vacuum", vacuumPoint.transform.position);
	}
}
