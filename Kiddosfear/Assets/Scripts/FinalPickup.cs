﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalPickup : MonoBehaviour {

    [Tooltip("Reference to the animator on the boss")]
    public Animator bossAnimController;
    [Tooltip("Determine where the camera zooms to during outro cinematic")]
    public GameObject camEndPosition;
    [Tooltip("Reference to the particleSystem")]
    [SerializeField] ParticleSystem paletteParticles;
    public float fadeStep;

    //References to other scripts used.
    private CameraAnimation camAnim;
    private UIGameManager uiControl;
    private CameraController camControl;
    private TouchPaint touchPaint;

    private Renderer palette;
    [SerializeField] Material particleMat;
    private ParticleSystem.MainModule ma;
    private bool panning = false;
    private bool fading = false;


	// Use this for initialization
	void Start () {
        bossAnimController.speed = 0;

        camAnim = FindObjectOfType<CameraAnimation>();
        uiControl = FindObjectOfType<UIGameManager>();
        camControl = FindObjectOfType<CameraController>();
        touchPaint = FindObjectOfType<TouchPaint>();
        palette = GetComponent<Renderer>();
        ma = paletteParticles.main;

    }
    

    private void OnTriggerEnter(Collider other) {
        paletteParticles.gameObject.SetActive(true); //Activates the particle system
        palette.enabled = false; //Disables the pickup renderer 
        if (!fading) {
            fading = true;
            StartCoroutine(FadeColors());
        }
        
        if (!panning) {
            panning = true;
            camAnim.Pan(GameManager.Player.transform, camEndPosition.transform, 5f, 0f, false); //Starting the camera pan

            //Stop the player from moving
            uiControl.MoveLeft(false);
            uiControl.MoveRight(false);

            //Start the boss animation
            bossAnimController.speed = 1;
            StartCoroutine(WaitForPan());
        }

    }

    /// <summary>
    /// Wait for camera panning to finish then fade to black
    /// </summary>
    /// <returns></returns>
    IEnumerator WaitForPan() {
        yield return new WaitForSeconds(0.2f);
        while (camAnim.isPanning) {
            yield return new WaitForEndOfFrame();
        }


        //Disable UI elements and the ability to paint
        uiControl.ShowGUI(false);
        touchPaint.enabled = false;
        
        camControl._world = camEndPosition;
        
        yield return new WaitForSeconds(2f);
        
        camAnim.BeginFade(0f, 1f, fadeStep);

        while (camAnim.isFading) {
            yield return new WaitForEndOfFrame();
        }

        SceneManager.LoadScene("Credits");
    }

    /// <summary>
    /// Fades the particleSystem from full color to grey
    /// </summary>
    /// <returns></returns>
    IEnumerator FadeColors() {

        //Set up initial timestamps
        float lerpStartTime = Time.time;
        float lerpDuration = 3.5f;
        float lerpProgress;
        int counter = 0;                //Failsafe in case of infinite loop

        Color endLerpColor = Color.grey;

        //Refereces to the gradient in startColor
        ParticleSystem.MinMaxGradient grad = ma.startColor;
        GradientColorKey[] newGradCol = grad.gradient.colorKeys;
        GradientColorKey[] lerpStartColors = grad.gradient.colorKeys;
        
        //References to the material color on the particles
        particleMat = paletteParticles.GetComponent<ParticleSystemRenderer>().material;
        Color particleMatColorStart = particleMat.color;

        //References to the emission color on the particles
        Color emissionStartColor = particleMat.GetColor("_EmissionColor");
        Color emissionColor = emissionStartColor;
        

        bool shouldLerp = true;
        while (shouldLerp && counter < 300) { //Lerps the color to grey
            yield return new WaitForEndOfFrame();

            lerpProgress = Time.time - lerpStartTime;

            //Change all the keys in the gradient towards grey
            for (int i = 0; i < newGradCol.Length; i++) {
                newGradCol[i].color = Color.Lerp(lerpStartColors[i].color, endLerpColor, lerpProgress / lerpDuration);
            }
            grad.gradient.colorKeys = newGradCol;
            ma.startColor = grad;

            //Fade the color of the particle material
            particleMat.color = Color.Lerp(particleMatColorStart, Color.grey, lerpProgress / lerpDuration);

            //Fade the color of the particle emission
            emissionColor = Color.Lerp(emissionStartColor, Color.grey, lerpProgress / lerpDuration);
            particleMat.SetColor("_EmissionColor", emissionColor);
            
            if (lerpProgress >= lerpDuration)
                shouldLerp = false;

            counter++;
        }

        StartCoroutine(FadeOut());
    }

    /// <summary>
    /// Fades the star particles so they become invisble
    /// </summary>
    /// <returns></returns>
    IEnumerator FadeOut() {
        float lerpStartTime = Time.time;
        float lerpDuration = 3.5f;
        float lerpProgress;
        int counter = 0;

        ParticleSystem.MinMaxGradient grad = ma.startColor;
        GradientAlphaKey[] newGradAlph = grad.gradient.alphaKeys;
        GradientAlphaKey[] lerpStartAlpha = grad.gradient.alphaKeys;

        bool shouldLerp = true;
        while (shouldLerp && counter < 300) { //Fades the start particles so they become invisble
            yield return new WaitForEndOfFrame();

            lerpProgress = Time.time - lerpStartTime;

            //Change all the alphakeys in the gradient towards invisible
            for (int i = 0; i < newGradAlph.Length; i++) {
                newGradAlph[i].alpha = Mathf.Lerp(lerpStartAlpha[i].alpha, 0f, lerpProgress / lerpDuration);
            }
            grad.gradient.alphaKeys = newGradAlph;
            ma.startColor = grad;

            if (lerpProgress >= lerpDuration)
                shouldLerp = false;

            counter++;
        }

        StartCoroutine(ScaleParticle());
    }

    /// <summary>
    /// Scales the particle system down so it dissapears
    /// </summary>
    /// <returns></returns>
    IEnumerator ScaleParticle() {

        float lerpStartTime = Time.time;
        float lerpDuration = 1.5f;
        float lerpProgress;
        int counter = 0;

        Vector3 startScale = paletteParticles.transform.localScale;
        Vector3 endScale = new Vector3(0, 0, 0);

        bool shouldLerp = true;
        while (shouldLerp && counter < 300) { //Lerps the scale down until it dissapears
            yield return new WaitForEndOfFrame();

            lerpProgress = Time.time - lerpStartTime;

            paletteParticles.transform.localScale = Vector3.Lerp(startScale, endScale, lerpProgress / lerpDuration);

            if (lerpProgress >= lerpDuration)
                shouldLerp = false;

            counter++;
        }


    }
}
