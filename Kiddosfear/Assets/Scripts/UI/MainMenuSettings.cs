﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuSettings : MonoBehaviour {

    public static MainMenuSettings instance;

    public static bool firstLaunch = true;
    public static string currentScene = "";
    public static string previousScene = "";

    private void Awake() {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        DontDestroyOnLoad(this);
    }

    public static void EndOfFirstLaunch() {
        firstLaunch = false;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        previousScene = currentScene;
        currentScene = scene.name;
    }

    private void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
