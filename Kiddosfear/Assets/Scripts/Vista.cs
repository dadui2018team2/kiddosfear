﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vista : MonoBehaviour {

    [Header("Referenes are set by the script")]
    [SerializeField] Transform mobileVistaPos;
    [SerializeField]Transform tabletVistaPos;
    [SerializeField] CameraController camControl;
    public bool isTablet = false;
    //[SerializeField] CameraAnimation camAnim;
    //[SerializeField] UIGameManager uiControl;
    //[SerializeField] TouchPaint touchPaint;

    private bool panning = false;
    float screenHeight;

    // Use this for initialization
    private void Awake () {
        float screenHeight = Screen.height / Screen.dpi;
        mobileVistaPos = gameObject.transform.GetChild(0).transform;
        tabletVistaPos = gameObject.transform.GetChild(1).transform;
        camControl = FindObjectOfType<CameraController>();
   

        //----------------saved in case we need more control during the camera panning---------------------------
        //camAnim = FindObjectOfType<CameraAnimation>();
        //uiControl = FindObjectOfType<UIGameManager>();
        //touchPaint = FindObjectOfType<TouchPaint>();
    }
    //Enable this for live version.

    //When the player enters the trigger. set the new focuspoint to the child of the vista gameobject
   /* private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player" && PlayerPrefs.GetFloat("DeviceType") == 0) {
            camControl._world = mobileVistaPos.gameObject;
            //Debug.Log("Mobile Vista chosen");
        }
        else if (other.tag == "Player" && PlayerPrefs.GetFloat("DeviceType") == 1)
        {
            camControl._world = tabletVistaPos.gameObject;
           // Debug.Log("Tablet Vista chosen");
        }*/


        //Use this for debugging (Lets the user control each individual vista mode
        private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && isTablet == false)
        {
            camControl._world = mobileVistaPos.gameObject;
            Debug.Log("Mobile Vista chosen");
        }
        else if (other.tag == "Player" && isTablet == true)
        {
            camControl._world = tabletVistaPos.gameObject;
            Debug.Log("Tablet Vista chosen");
        }

        //----------------saved in case we need more control during the camera panning---------------------------
        /*if (!panning) {
            camAnim.Pan(gameObject.transform, vistaPos, 0f, 0f);
            uiControl.ShowGUI(true);
            touchPaint.enabled = true;
            StartCoroutine(WaitForPan());
        }*/

    }

    //When the player exits the trigger set the focuspoint back to the player
    private void OnTriggerExit(Collider other) {
        if (other.tag == "Player") {
            camControl._world = GameManager.Player;
        }
    }


    //----------------saved in case we need more control during the camera panning---------------------------
    /*IEnumerator WaitForPan() {
        panning = true;
        int counter = 0;
        while (camAnim.isPanning || counter < 100) {
            yield return new WaitForEndOfFrame();
            counter++;
        }
        counter = 0;
        camControl._world = vistaPos.gameObject;
    }*/


}
