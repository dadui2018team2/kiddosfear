﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateArt : MonoBehaviour {

    public string artType;

    private Animator animatorController;
    private bool statueTurned = false;
    private bool ballDeflated = false;
    private bool loadBank = true;

	// Use this for initialization
	void Start () {
		
	}

    /// <summary>
    /// Determines which art type this object is based on a string in the inspector. Calls function based on art type
    /// </summary>
    /// <param name="color">Specify which color should activate this animation. If all colors write any color</param>
    public void DetermineArtType(string color) {
        switch (artType) {
            case "Ball":
                if (color == "Blue")
                    JumpingBall();
                break;
            case "Statue":
                if (color == "")
                    TurnStatueBack();
                else
                    TurnStatue();
                break;
            case "Door":
                if (color == "")
                    OpenDoor(false);
                else
                    OpenDoor(true);
                break;
            case "DoorFrame":
                if (color == "")
                    DoorMovement(false, color);
                else
                    DoorMovement(true, color);
                break;
            case "BallOut":
                BallSlideOut();
                break;
            case "BallDeflate":
                if (color == "")
                    BallInflate();
                else
                    BallDeflate();
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Plays animation for rolling the ball off the edge in level 1
    /// </summary>
    private void JumpingBall() {
        animatorController = GetComponent<Animator>();
        AudioPlayer.LoadBank("Animations");
        AkSoundEngine.PostEvent("BallFalling", gameObject);
        animatorController.Play("BouncyBounce");

        StartCoroutine(WaitForAnim("BouncyBounce", true));
    }

    /// <summary>
    /// Turns the statue in level 1 when it is colored
    /// </summary>
    private void TurnStatue() {
        statueTurned = true;
        animatorController = GetComponent<Animator>();
        AudioPlayer.LoadBank("Animations");
        AkSoundEngine.PostEvent("Statue", gameObject);
        StartCoroutine(QueueAnimation("StatueTurnBack", "StatueTurn", "Statue"));
        
    }

    /// <summary>
    /// Turns the statue back when color is removed from it
    /// </summary>
    private void TurnStatueBack() {
        if (statueTurned) {
            statueTurned = false;
            animatorController = GetComponent<Animator>();
            AudioPlayer.LoadBank("Animations");
            AkSoundEngine.PostEvent("Statue", gameObject);
            StartCoroutine(QueueAnimation("StaturTurn", "StatueTurnBack", "Statue"));
        }
    }

    private void BallDeflate() {
        Debug.Log("Deflating ball");
        ballDeflated = true;
        animatorController = GetComponent<Animator>();
        AudioPlayer.LoadBank("Animations");
        AkSoundEngine.PostEvent("BallNoAir", gameObject);

        StartCoroutine(QueueAnimation("BallInflate", "BallDeflate", "BallNoAir"));
    }

    private void BallInflate() {
        if (ballDeflated) {
            Debug.Log("Inflateing ball");
            ballDeflated = false;
            animatorController = GetComponent<Animator>();
            AudioPlayer.LoadBank("Animations");
            AkSoundEngine.PostEvent("BallNoAir", gameObject);
            StartCoroutine(QueueAnimation("BallDeflate", "BallInflate", "BallNoAir"));
        }
    }

    /// <summary>
    /// Opens and closes the door on the animatedDoorParent prefab
    /// </summary>
    /// <param name="stop"></param>
    private void OpenDoor(bool stop) {
        animatorController = GetComponent<Animator>();
        if (loadBank == true) {
            AudioPlayer.LoadBank("TeleDoor");
            loadBank = false;
            
        }

        animatorController.Play("DoorOpen");

        if (stop) {
            animatorController.speed = 1;
            
        }
        else {
            animatorController.speed = 0;
        }
    }

    private void OpenDoorSound() {
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorOpen", gameObject);
        AkSoundEngine.PostEvent("TeleDoor", gameObject);
    }

    private void CloseDoorSound() {
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorCloses", gameObject);
        AkSoundEngine.PostEvent("TeleDoor", gameObject);
    }

    /// <summary>
    /// Moves the door up and down if colored blue and side to side if colored green
    /// </summary>
    /// <param name="stop">If the color is clear the animation will pause</param>
    /// <param name="color">Green for side to side, blue for up and down</param>
    private void DoorMovement(bool stop, string color) {
        animatorController = GetComponent<Animator>();

        if (color == "Blue")
            animatorController.Play("DoorMovement");
        else if (color == "Green")
            animatorController.Play("DoorMovementSide");

        if (stop)
            animatorController.speed = 1;
        else
            animatorController.speed = 0;
    }

    private void BallSlideOut() {
        animatorController = GetComponent<Animator>();
        AudioPlayer.LoadBank("Animations");
        AkSoundEngine.PostEvent("BallSpinning", gameObject);
        animatorController.Play("BallSlideOut");

        StartCoroutine(WaitForAnim("BallSlideOut", true));
    }

    private void PlayEvent(string soundEvent) {
        AkSoundEngine.PostEvent(soundEvent, gameObject);
    }

    /// <summary>
    /// Used to check of the current animation has finished playing 
    /// </summary>
    /// <param name="animName">The name of the animation</param>
    /// <returns></returns>
    private bool HasFinishedAnim(string animName) {
        if (animatorController.GetBool(animName) == false && animatorController.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.95f)
            return true;
        else
            return false;
    }

    /// <summary>
    /// Used to wait for a given animation to finish
    /// </summary>
    /// <param name="animation">Name of animation</param>
    /// <returns></returns>
    IEnumerator WaitForAnim(string animation, bool shouldDisable) {
        yield return new WaitForSeconds(0.2f);
        while (HasFinishedAnim(animation) == false) {
            yield return new WaitForEndOfFrame();
        }

        if(shouldDisable)
            DisableObject();

        AudioPlayer.UnloadBank("Animations", 3f);
    }

    /// <summary>
    /// Waits for the current animation to finish then plays the next one after
    /// </summary>
    /// <param name="currentAnimation">The animation that is currently playing</param>
    /// <param name="nextAnimation"> The animation that should be Queued</param>
    /// <returns></returns>
    IEnumerator QueueAnimation(string currentAnimation, string nextAnimation, string soundID) {
        yield return new WaitForSeconds(0.2f);
        while(HasFinishedAnim(currentAnimation) == false) {
            yield return new WaitForEndOfFrame();
        }
        
        animatorController.Play(nextAnimation);

        StartCoroutine(WaitForAnim(nextAnimation, false));
    }

    /// <summary>
    /// Disables the gameobject
    /// </summary>
    private void DisableObject() {
        gameObject.SetActive(false);
    }
}
