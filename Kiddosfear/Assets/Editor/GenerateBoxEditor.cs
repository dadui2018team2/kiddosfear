﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GenerateBox))]
public class GenerateBoxEditor : Editor {

	public string prefabLoadPath = "Assets/Prefabs/Environment";

	public override void OnInspectorGUI() {
		GenerateBox gb = (GenerateBox)target;

		EditorGUILayout.LabelField("Generate a box with the given prefab and settings.", EditorStyles.boldLabel);
		prefabLoadPath = EditorGUILayout.TextField("Prefab load path", prefabLoadPath);
		if(GUILayout.Button("Refresh prefab list")){
			gb.UpdatePrefabs(prefabLoadPath);
		}
		EditorGUILayout.Space();

		string[] prefabOptions = new string[gb.prefabs.Length];
		for (int i = 0; i < prefabOptions.Length; i++) {
			prefabOptions[i] = gb.prefabs[i].name;
		}
		gb.prefabIndex = EditorGUILayout.Popup("Prefab selection", gb.prefabIndex, prefabOptions);
		EditorGUILayout.Space();

		gb.size = EditorGUILayout.Vector3IntField("Size of box", gb.size);
		gb.position = EditorGUILayout.Vector3Field("Position", gb.position);
		gb.spacing = EditorGUILayout.FloatField("Spacing", gb.spacing);
		gb.shell = EditorGUILayout.Toggle("Empty", gb.shell);

		if (GUILayout.Button("Generate box")){
			gb.Generate();
		}
	}
}
