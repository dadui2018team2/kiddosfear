﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteraction : MonoBehaviour {

    [Header("Updated in awake")]
    public TeleDoorManager teleDoorManager;
    public Animator animator;

    [Header("Door variables (do not touch)")]
    public bool isActive;
    public bool isOpen;
    public GameObject otherDoor;

    private void Awake() {
        teleDoorManager = transform.parent.GetComponent<TeleDoorManager>();
        animator = GetComponent<Animator>();

    }

    /// <summary>
    /// Checks if the current door is active. If it isn't, it will activate.
    /// </summary>
    public void AttemptActivateDoor() {
        if (!isActive) {
            if (teleDoorManager.DoorIsNewCheckpoint(gameObject)) {
                teleDoorManager.ActivateDoors(this);
                int doorValue = (int)char.GetNumericValue(gameObject.name[gameObject.name.Length - 1]);
                teleDoorManager.SaveDoorCheckpoint(doorValue);
            }
        }
    }

    /// <summary>
    /// Activates the door.
    /// </summary>
    public void ActivateDoor() {

        animator.Play("DoorActivate");
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorAppear", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
        isActive = true;
    }

    /// <summary>
    /// Opens the door (Should only be called after activating!)
    /// </summary>
    public void OpenDoor() {
        if (!isOpen)
            animator.Play("OpenDoor");
        isOpen = true;
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorOpen", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    /// <summary>
    /// Closes the door.
    /// </summary>
    public void CloseDoor() {
        animator.Play("CloseDoor");
        isOpen = false;
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorClose", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    /// <summary>
    /// Sets which door to teleport to, when entering the door.
    /// </summary>
    /// <param name="door"></param>
    public void SetOtherDoor(GameObject door) {
        otherDoor = door;
    }

    /// <summary>
    /// Activates and opens the door.
    /// </summary>
    public void ActivateAndOpenDoor() {
        ActivateDoor();
        Invoke("OpenDoor", 1f);
    }

    /// <summary>
    /// Invokes private or public function in the script.
    /// </summary>
    /// <param name="functionName">Name of the function to run</param>
    /// <param name="time">Delay before running the function</param>
    public void InvokeClassFunction(string functionName, float time) {
        Invoke(functionName, time);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            DoorInteraction otherDoorInteraction = otherDoor.GetComponent<DoorInteraction>();

            if (otherDoor == null) Debug.Log("Can't find 'otherDoor' in DoorInteraction.cs. Current door: " + gameObject.name);

            otherDoorInteraction.OpenDoor();
            other.transform.position = otherDoorInteraction.gameObject.transform.position + (otherDoorInteraction.gameObject.transform.forward * 2);
            AudioPlayer.SetSwitch("TeleDoor", "TeleDoorKiddoWalksThrough", otherDoor.gameObject);
            AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
            otherDoorInteraction.Invoke("CloseDoor", 1f);
        }
    }
}




    /*public GameObject partnerDoor;
    public bool canActivateDoor;
    public float doorTeleportOffset;

    private void Start() {
        if (gameObject.name == "DoorActivator") {
            canActivateDoor = true;
        }
        else {
            canActivateDoor = false;
        }
    }

    private void ActivateDoor() {
        gameObject.GetComponent<Renderer>().material.color = Color.black;
        gameObject.GetComponent<BoxCollider>().isTrigger = true;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            if (canActivateDoor) {
                gameObject.GetComponent<BoxCollider>().enabled = false;
                foreach (Transform child in transform.parent.parent) {
                    if (child.GetComponentInChildren<DoorInteraction>() != null) {
                        child.GetComponentInChildren<DoorInteraction>().ActivateDoor();
                    }
                }
            }
            else if (gameObject.name == "Opening") {
                other.transform.position = partnerDoor.transform.position + (partnerDoor.transform.forward * doorTeleportOffset) ;
                Debug.Log("MMMMMMMMMMH CONTACT WITH HUMAN INITIATED! RUB YOU CAPSULE BODY UP AND DOWN MY FRAME BABY!");
            }
            else {
                print("Found unknown dobject: " + gameObject.name);
            }
        }
    }
}*/
