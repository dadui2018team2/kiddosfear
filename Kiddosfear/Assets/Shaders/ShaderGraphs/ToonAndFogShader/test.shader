﻿Shader "Multiply"
{
    Properties
    {
        [NoScaleOffset] _Albedo("AlbedoMap", 2D) = "white" {}
        _Tint("Tint", Color) = (1,1,1,0)
        [Toggle] Boolean_502B7F59("UseEmissionMap", Float) = 0
        Vector1_36B66B54("EmissionMultiplier", Float) = 1
        [NoScaleOffset] Texture2D_7254517C("EmissionMap", 2D) = "white" {}
        [Toggle] Boolean_CB3ADE1F("UseFog", Float) = 1
        Color_6A0F145F("FogColour", Color) = (0,0,0,0)
        Vector1_162CC5A5("FogStart", Float) = 0
        Vector1_4633FC98("FogEnd", Float) = 150
        [Toggle] Boolean_1C5F59C9("UseHeightFog", Float) = 0
        Color_C7E9DB98("HeightFogColour", Color) = (0,0,0,0)
        Vector1_B3A9C87("HeightFogStart", Float) = 0
        Vector1_9A8DB970("HeightFogEnd", Float) = 10
        [Toggle] Boolean_3E63768F("UseHeightFogSprite", Float) = 0
        [NoScaleOffset] Texture2D_2AE0E5A9("HeightFogSprite", 2D) = "white" {}
        Vector2_CF7900C3("HeightFogSpriteTiling", Vector) = (1,1,0,0)
        Vector2_A43D293B("HeightFogSpriteOffset", Vector) = (0,0,0,0)
        Vector1_401C562B("AlphaClipThreshold", Range(0, 1)) = 0
        _Alpha("Alpha", Range(0, 1)) = 1
        [Toggle] _UseVacuum("UseVacuum", Float) = 0
        _Vacuum("Vacuum", Vector) = (0,0,0,0)
        _VacuumStart("VacuumStart", Float) = 0
        _VacuumEnd("VacuumEnd", Float) = 150
    }

    HLSLINCLUDE
    #define USE_LEGACY_UNITY_MATRIX_VARIABLES
    #include "CoreRP/ShaderLibrary/Common.hlsl"
    #include "CoreRP/ShaderLibrary/Packing.hlsl"
    #include "CoreRP/ShaderLibrary/Color.hlsl"
    #include "CoreRP/ShaderLibrary/UnityInstancing.hlsl"
    #include "CoreRP/ShaderLibrary/EntityLighting.hlsl"
    #include "ShaderGraphLibrary/ShaderVariables.hlsl"
    #include "ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"
    #include "ShaderGraphLibrary/Functions.hlsl"

    TEXTURE2D(_Albedo); SAMPLER(sampler_Albedo);
    float4 _Tint;
    float Boolean_502B7F59;
    float Vector1_36B66B54;
    TEXTURE2D(Texture2D_7254517C); SAMPLER(samplerTexture2D_7254517C);
    float Boolean_CB3ADE1F;
    float4 Color_6A0F145F;
    float Vector1_162CC5A5;
    float Vector1_4633FC98;
    float Boolean_1C5F59C9;
    float4 Color_C7E9DB98;
    float Vector1_B3A9C87;
    float Vector1_9A8DB970;
    float Boolean_3E63768F;
    TEXTURE2D(Texture2D_2AE0E5A9); SAMPLER(samplerTexture2D_2AE0E5A9);
    float2 Vector2_CF7900C3;
    float2 Vector2_A43D293B;
    float Vector1_401C562B;
    float _Alpha;
    float _UseVacuum;
    float3 _Vacuum;
    float _VacuumStart;
    float _VacuumEnd;
    struct SurfaceDescriptionInputs
    {
        float3 WorldSpaceNormal;
        float3 TangentSpaceNormal;
        float3 WorldSpaceTangent;
        float3 WorldSpaceBiTangent;
        float3 WorldSpacePosition;
        half4 uv0;
    };


    void Unity_Distance_float3(float3 A, float3 B, out float Out)
    {
        Out = distance(A, B);
    }

    void Unity_InverseLerp_float(float A, float B, float T, out float Out)
    {
        Out = (T - A)/(B - A);
    }

    void Unity_Clamp_float(float In, float Min, float Max, out float Out)
    {
        Out = clamp(In, Min, Max);
    }

    void Unity_Lerp_float4(float4 A, float4 B, float4 T, out float4 Out)
    {
        Out = lerp(A, B, T);
    }

    void Unity_Branch_float4(float Predicate, float4 True, float4 False, out float4 Out)
    {
        Out = lerp(False, True, Predicate);
    }

    void Unity_Multiply_float (float4 A, float4 B, out float4 Out)
    {
        Out = A * B;
    }

    void CustomFunction_float(out float3 Direction, out float Attenuation, out float3 Color, float3 WorldPos)
    {
    			Light mainLight = GetMainLight();
    			Color = mainLight.color;
    			Direction = mainLight.direction;
    			float4 shadowCoord;
    			#ifdef _SHADOWS_ENABLED
            	#if SHADOWS_SCREEN
    				float4 clipPos = TransformWorldToHClip(WorldPos);
            		shadowCoord = ComputeShadowCoord(clipPos);
            	#else
            		shadowCoord = TransformWorldToShadowCoord(WorldPos);
            	#endif
    			mainLight.attenuation = MainLightRealtimeShadowAttenuation(shadowCoord);
            	#endif
    			Attenuation = mainLight.attenuation;
    		}

    void Unity_Multiply_float (float3 A, float3 B, out float3 Out)
    {
        Out = A * B;
    }

    void Unity_Normalize_float3(float3 In, out float3 Out)
    {
        Out = normalize(In);
    }

    void Unity_NormalBlend_float(float3 A, float3 B, out float3 Out)
    {
        Out = normalize(float3(A.rg + B.rg, A.b * B.b));
    }

    void Unity_MatrixConstruction_float (float4 M0, float4 M1, float4 M2, float4 M3, out float4x4 Out4x4, out float3x3 Out3x3, out float2x2 Out2x2)
    {
        Out4x4 = float4x4(M0.x, M0.y, M0.z, M0.w, M1.x, M1.y, M1.z, M1.w, M2.x, M2.y, M2.z, M2.w, M3.x, M3.y, M3.z, M3.w);
        Out3x3 = float3x3(M0.x, M0.y, M0.z, M1.x, M1.y, M1.z, M2.x, M2.y, M2.z);
        Out2x2 = float2x2(M0.x, M0.y, M1.x, M1.y);
    }

    void Unity_Multiply_float (float3x3 A, float3 B, out float3 Out)
    {
        Out = mul(A, B);
    }

    // Subgraph function
    void sg_Tanget2World_39A17821(float3 Vector3_185701D8, SurfaceDescriptionInputs IN, out float4 Output1)
    {
        float4x4 _MatrixConstruction_C2983B8A_4x4;
        float3x3 _MatrixConstruction_C2983B8A_3x3;
        float2x2 _MatrixConstruction_C2983B8A_2x2;
        Unity_MatrixConstruction_float((float4(IN.WorldSpaceTangent, 1.0)), (float4(IN.WorldSpaceBiTangent, 1.0)), (float4(IN.WorldSpaceNormal, 1.0)), float4 (0,0,0,0), _MatrixConstruction_C2983B8A_4x4, _MatrixConstruction_C2983B8A_3x3, _MatrixConstruction_C2983B8A_2x2);
        
        float3 _Property_EE2A9B7B_Out = Vector3_185701D8;
        float3 _Multiply_CF85B1BB_Out;
        Unity_Multiply_float(_MatrixConstruction_C2983B8A_3x3, _Property_EE2A9B7B_Out, _Multiply_CF85B1BB_Out);
        
        Output1 = (float4(_Multiply_CF85B1BB_Out, 1.0));
    }

    void Unity_DotProduct_float3(float3 A, float3 B, out float Out)
    {
        Out = dot(A, B);
    }

    void Unity_Smoothstep_float(float Edge1, float Edge2, float In, out float Out)
    {
        Out = smoothstep(Edge1, Edge2, In);
    }

    void Unity_Remap_float(float In, float2 InMinMax, float2 OutMinMax, out float Out)
    {
        Out = OutMinMax.x + (In - InMinMax.x) * (OutMinMax.y - OutMinMax.x) / (InMinMax.y - InMinMax.x);
    }

    void Unity_Add_float4(float4 A, float4 B, out float4 Out)
    {
        Out = A + B;
    }

    struct SurfaceDescription
    {
        float3 Out;
    };

    SurfaceDescription PopulateSurfaceData(SurfaceDescriptionInputs IN)
    {
        SurfaceDescription surface = (SurfaceDescription)0;
        float _Vector1_D2B37E5A_Out = 1.5;
        float _Property_3886A595_Out = _UseVacuum;
        float4 Color_397B39CC = IsGammaSpace() ? float4(0.3686275, 0.3686275, 0.3686275, 0) : float4(SRGBToLinear(float3(0.3686275, 0.3686275, 0.3686275)), 0);
        float4 _Property_487F642F_Out = _Tint;
        float _Property_5613298B_Out = _VacuumStart;
        float _Property_CF4CAFB3_Out = _VacuumEnd;
        float3 _Property_C6CA9F8D_Out = _Vacuum;
        float _Distance_7285BE5D_Out;
        Unity_Distance_float3(IN.WorldSpacePosition, _Property_C6CA9F8D_Out, _Distance_7285BE5D_Out);
        float _InverseLerp_F8467482_Out;
        Unity_InverseLerp_float(_Property_5613298B_Out, _Property_CF4CAFB3_Out, _Distance_7285BE5D_Out, _InverseLerp_F8467482_Out);
        float _Clamp_A326ECE3_Out;
        Unity_Clamp_float(_InverseLerp_F8467482_Out, 0, 1, _Clamp_A326ECE3_Out);
        float4 _Lerp_890DDFAF_Out;
        Unity_Lerp_float4(Color_397B39CC, _Property_487F642F_Out, (_Clamp_A326ECE3_Out.xxxx), _Lerp_890DDFAF_Out);
        float4 _Branch_71D78FF4_Out;
        Unity_Branch_float4(_Property_3886A595_Out, _Lerp_890DDFAF_Out, _Property_487F642F_Out, _Branch_71D78FF4_Out);
        float4 _SampleTexture2D_2E765A74_RGBA = SAMPLE_TEXTURE2D(_Albedo, sampler_Albedo, IN.uv0.xy);
        float _SampleTexture2D_2E765A74_R = _SampleTexture2D_2E765A74_RGBA.r;
        float _SampleTexture2D_2E765A74_G = _SampleTexture2D_2E765A74_RGBA.g;
        float _SampleTexture2D_2E765A74_B = _SampleTexture2D_2E765A74_RGBA.b;
        float _SampleTexture2D_2E765A74_A = _SampleTexture2D_2E765A74_RGBA.a;
        float4 _Multiply_2ADF5022_Out;
        Unity_Multiply_float(_Branch_71D78FF4_Out, _SampleTexture2D_2E765A74_RGBA, _Multiply_2ADF5022_Out);

        float3 _MainLight_DE9C80BC_Direction;
        float _MainLight_DE9C80BC_Attenuation;
        float3 _MainLight_DE9C80BC_Color;
        CustomFunction_float(_MainLight_DE9C80BC_Direction, _MainLight_DE9C80BC_Attenuation, _MainLight_DE9C80BC_Color, IN.WorldSpacePosition);
        float3 _Multiply_BF04D2C2_Out;
        Unity_Multiply_float((_Multiply_2ADF5022_Out.xyz), _MainLight_DE9C80BC_Color, _Multiply_BF04D2C2_Out);

        float3 _Normalize_EDEA06BA_Out;
        Unity_Normalize_float3(_MainLight_DE9C80BC_Direction, _Normalize_EDEA06BA_Out);
        float4 Color_8921549D = IsGammaSpace() ? float4(0, 0, 1, 0) : float4(SRGBToLinear(float3(0, 0, 1)), 0);
        float3 _NormalBlend_D3437CB3_Out;
        Unity_NormalBlend_float((Color_8921549D.xyz), IN.TangentSpaceNormal, _NormalBlend_D3437CB3_Out);
        float4 _Subgraph_71417F1_Output1;
        sg_Tanget2World_39A17821(_NormalBlend_D3437CB3_Out, IN, _Subgraph_71417F1_Output1);
        float _DotProduct_B1B345C2_Out;
        Unity_DotProduct_float3(_Normalize_EDEA06BA_Out, (_Subgraph_71417F1_Output1.xyz), _DotProduct_B1B345C2_Out);
        float _Smoothstep_32CD8C5A_Out;
        Unity_Smoothstep_float(0, 0.11, _DotProduct_B1B345C2_Out, _Smoothstep_32CD8C5A_Out);
        float _Remap_DBC4DE79_Out;
        Unity_Remap_float(_Smoothstep_32CD8C5A_Out, float2 (-1,1), float2 (0,1), _Remap_DBC4DE79_Out);
        float3 _Multiply_8A261BA1_Out;
        Unity_Multiply_float(_Multiply_BF04D2C2_Out, (_Remap_DBC4DE79_Out.xxx), _Multiply_8A261BA1_Out);

        float3 _Multiply_C505BD8E_Out;
        Unity_Multiply_float((_Vector1_D2B37E5A_Out.xxx), _Multiply_8A261BA1_Out, _Multiply_C505BD8E_Out);

        float _Property_67663A1E_Out = Boolean_502B7F59;
        float _Vector1_4BDDDF9C_Out = 0;
        float4 _SampleTexture2D_79867D57_RGBA = SAMPLE_TEXTURE2D(Texture2D_7254517C, samplerTexture2D_7254517C, IN.uv0.xy);
        float _SampleTexture2D_79867D57_R = _SampleTexture2D_79867D57_RGBA.r;
        float _SampleTexture2D_79867D57_G = _SampleTexture2D_79867D57_RGBA.g;
        float _SampleTexture2D_79867D57_B = _SampleTexture2D_79867D57_RGBA.b;
        float _SampleTexture2D_79867D57_A = _SampleTexture2D_79867D57_RGBA.a;
        float _Property_AF93C19F_Out = Vector1_36B66B54;
        float4 _Multiply_DCF40375_Out;
        Unity_Multiply_float(_SampleTexture2D_79867D57_RGBA, (_Property_AF93C19F_Out.xxxx), _Multiply_DCF40375_Out);

        float4 _Add_2BB7825A_Out;
        Unity_Add_float4((_Vector1_4BDDDF9C_Out.xxxx), _Multiply_DCF40375_Out, _Add_2BB7825A_Out);
        float4 _Branch_AB762BC9_Out;
        Unity_Branch_float4(_Property_67663A1E_Out, _Add_2BB7825A_Out, (_Property_AF93C19F_Out.xxxx), _Branch_AB762BC9_Out);
        float3 _Multiply_3CF22416_Out;
        Unity_Multiply_float(_Multiply_C505BD8E_Out, (_Branch_AB762BC9_Out.xyz), _Multiply_3CF22416_Out);

        surface.Out = _Multiply_3CF22416_Out;
        return surface;
    }

    struct GraphVertexInput
    {
        float4 vertex : POSITION;
        float3 normal : NORMAL;
        float4 tangent : TANGENT;
        float4 texcoord0 : TEXCOORD0;
        UNITY_VERTEX_INPUT_INSTANCE_ID
    };

    GraphVertexInput PopulateVertexData(GraphVertexInput v)
    {
        return v;
    }

    ENDHLSL

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            struct GraphVertexOutput
            {
                float4 position : POSITION;
                float3 WorldSpacePosition : TEXCOORD0;
            float3 WorldSpaceNormal : TEXCOORD1;
            float3 WorldSpaceTangent : TEXCOORD2;
            float3 WorldSpaceBiTangent : TEXCOORD3;
            half4 uv0 : TEXCOORD4;

            };

            GraphVertexOutput vert (GraphVertexInput v)
            {
                v = PopulateVertexData(v);

                GraphVertexOutput o;
                float3 positionWS = TransformObjectToWorld(v.vertex);
                o.position = TransformWorldToHClip(positionWS);
                float3 WorldSpacePosition = mul(UNITY_MATRIX_M,v.vertex);
            float3 WorldSpaceNormal = mul(v.normal,(float3x3)UNITY_MATRIX_I_M);
            float3 WorldSpaceTangent = mul((float3x3)UNITY_MATRIX_M,v.tangent.xyz);
            float3 WorldSpaceBiTangent = normalize(cross(WorldSpaceNormal, WorldSpaceTangent.xyz) * v.tangent.w);
            float4 uv0 = v.texcoord0;
                o.WorldSpacePosition = WorldSpacePosition;
                o.WorldSpaceNormal = WorldSpaceNormal;
                o.WorldSpaceTangent = WorldSpaceTangent;
                o.WorldSpaceBiTangent = WorldSpaceBiTangent;
                o.uv0 = uv0;

                return o;
            }

            float4 frag (GraphVertexOutput IN ) : SV_Target
            {
                float3 WorldSpacePosition = IN.WorldSpacePosition;
            float3 WorldSpaceNormal = normalize(IN.WorldSpaceNormal);
            float3 WorldSpaceTangent = IN.WorldSpaceTangent;
            float3 WorldSpaceBiTangent = IN.WorldSpaceBiTangent;
            float3x3 tangentSpaceTransform = float3x3(WorldSpaceTangent,WorldSpaceBiTangent,WorldSpaceNormal);
            float4 uv0 = IN.uv0;
            float3 TangentSpaceNormal = mul(WorldSpaceNormal,(float3x3)tangentSpaceTransform);

                SurfaceDescriptionInputs surfaceInput = (SurfaceDescriptionInputs)0;;
                surfaceInput.WorldSpaceNormal = WorldSpaceNormal;
            surfaceInput.TangentSpaceNormal = TangentSpaceNormal;
            surfaceInput.WorldSpaceTangent = WorldSpaceTangent;
            surfaceInput.WorldSpaceBiTangent = WorldSpaceBiTangent;
            surfaceInput.WorldSpacePosition = WorldSpacePosition;
            surfaceInput.uv0 = uv0;

                SurfaceDescription surf = PopulateSurfaceData(surfaceInput);
                return half4(surf._Multiply_3CF22416_Out.x, surf._Multiply_3CF22416_Out.y, surf._Multiply_3CF22416_Out.z, 1.0);

            }
            ENDHLSL
        }
    }
}
