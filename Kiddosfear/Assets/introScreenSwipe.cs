﻿using UnityEngine;
using UnityEngine.UI;

public class introScreenSwipe : MonoBehaviour {

    public float timeScale;

    Image image;
    Color originalImageColor;
    Text titleText;
    Color originalTitleTextColor;
    Text swipeText;
    Color originalSwipeTextColor;

    private void Start() {
        image = GetComponent<Image>();
        Text[] textElements = GetComponentsInChildren<Text>();
        foreach (Text text in textElements) {
            if (text.gameObject.name.ToLower().Contains("title")) {
                titleText = text;
            }
            else if (text.gameObject.name.ToLower().Contains("swipe")) {
                swipeText = text;
            }
        }
        originalImageColor = image.color;
        originalTitleTextColor = titleText.color;
        originalSwipeTextColor = swipeText.color;
    }

    public void ReduceVisibility() {
        Color newColor = image.color;
        newColor.a -= Time.deltaTime * timeScale;
        image.color = newColor;

        newColor = titleText.color;
        newColor.a -= Time.deltaTime * timeScale;
        titleText.color = newColor;

        newColor = swipeText.color;
        newColor.a -= Time.deltaTime * timeScale;
        swipeText.color = newColor;
    }

    public void ResetVisibility() {
        if (image.color.a > 0.1f) {
            image.color = originalImageColor;
            titleText.color = originalTitleTextColor;
            swipeText.color = originalImageColor;
        }
        else {
            gameObject.SetActive(false);
        }
    }

}
