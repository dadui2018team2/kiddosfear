﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemPickupUIManager : MonoBehaviour {

    [Header("Health icons")]
    [Tooltip("Duration for the fadein of health icon up life gain")]
    public float fadeDuration;
    [Tooltip("Is set automatically upon start")]
    public Image[] heartIcons;
    [Tooltip("Is set automatically upon start")]
    public List<Image> heartIconList = new List<Image>();
    
    [Header("pickup counter")]
    [Tooltip("Is set automatically upon start")]
    public Text pickupCounter;

    private void Start() {
        heartIcons = GetComponentsInChildren<Image>();
        pickupCounter = GetComponentInChildren<Text>();

        heartIconList.AddRange(heartIcons);

        //Removes all non heart icons from the list
        for(int i = 0; i < heartIconList.Count; i++) {
            if (!heartIconList[i].transform.parent.GetComponent<ItemPickupUIManager>()) {
                heartIconList.Remove(heartIconList[i]);
            }
        }
    }

    /// <summary>
    /// Activates health icon UI elements when the player gains a life
    /// </summary>
    /// <param name="currentLife"></param>
    public void AddLife(int currentLife) {
        heartIconList[currentLife - 1].gameObject.SetActive(true);
        StartCoroutine(FadeIcon(heartIconList[currentLife-1]));

        
    }

    /// <summary>
    /// Deactivates healt icon UI elements when the player loses a life
    /// </summary>
    /// <param name="currentLife"></param>
    public void RemoveLife(int currentLife) {
        heartIconList[currentLife].gameObject.SetActive(false);
        Color fadedColor = heartIconList[currentLife].color;
        fadedColor.a = 0;
        heartIconList[currentLife].color = fadedColor;

    }

    /// <summary>
    /// Updates the pickup counter textelemt whenever the player picks up a pickup
    /// </summary>
    /// <param name="counter"></param>
    public void UpdatePickup(int counter) {
        pickupCounter.text = counter.ToString();
    }

    /// <summary>
    /// Activates all health icons
    /// </summary>
    public void ResetLife() {
        for (int i = 0; i < heartIconList.Count; i++) {
            Color initColor = heartIconList[i].color;
            initColor.a = 1;
            heartIconList[i].color = initColor;
            heartIconList[i].gameObject.SetActive(true);

        }
    }

    /// <summary>
    /// Used to fade the life icons in when the player gains a life
    /// </summary>
    /// <param name="iconToFade"></param>
    /// <returns></returns>
    IEnumerator FadeIcon(Image iconToFade) {
        float fadeStartTime = Time.time;
        float fadeProgress;
        Color initColor = iconToFade.color;
        initColor.a = 1;

        Color fadeColor = iconToFade.color;

        bool shouldFade = true;
        while (shouldFade) {
            yield return new WaitForEndOfFrame();

            fadeProgress = Time.time - fadeStartTime;
            if (iconToFade != null)
                iconToFade.color = Color.Lerp(fadeColor, initColor, fadeProgress / fadeDuration);
            else
                shouldFade = false;
            
            if (fadeProgress >= fadeDuration)
                shouldFade = false;
        }
        
    }
    
}
