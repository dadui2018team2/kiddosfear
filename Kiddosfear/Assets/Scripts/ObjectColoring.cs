﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectColoring : MonoBehaviour {

    public enum Type {
        Cube,
        Wedge,
        AnimatedProp,
        StaticProp,
        SeaCreature,
        None
    }

    public Type type = Type.None;
    [HideInInspector] public string cubeType;

    [Header("Block meshes")]
    [SerializeField] GameObject noColor;
    [SerializeField] GameObject red;
    [SerializeField] GameObject green;
    [SerializeField] GameObject blue;

    private string currentColor;

    private Collider col;
    private Material mat;
    private Texture textureDefault;
    private Color colorDefault;

    private SeaCreature seaCreature;
    private AnimateArt animateArt;

    private void Awake() {
        if (type == Type.None) {
            Debug.LogError(gameObject.name + " has not been given a type in CubeColoring.");
            return;
        }

        if (type == Type.Cube) {
            col = GetComponent<Collider>();
            cubeType = "Cube";

            if (gameObject.tag == "Red" || gameObject.tag == "Green" || gameObject.tag == "Blue") {
                currentColor = gameObject.tag;
            }
            else if (gameObject.tag == "Cube") {
                currentColor = "";
            }

            return;
        }

        if (type == Type.Wedge) {
            col = GetComponent<Collider>();
            cubeType = "Wedge";

            if (gameObject.tag == "Red" || gameObject.tag == "Green" || gameObject.tag == "Blue") {
                currentColor = gameObject.tag;
            }
            else if (gameObject.tag == "Wedge") {
                currentColor = "";
            }

            return;
        }

        if (type == Type.AnimatedProp)
            animateArt = GetComponent<AnimateArt>();
        else if (type == Type.SeaCreature)
            seaCreature = GetComponent<SeaCreature>();

        //If we've made it this far, type is either AnimatedProp, StaticProp or SeaCreate. Therefore, get material properties to use for re-coloring.
        mat = GetComponentInChildren<Renderer>().material;
        textureDefault = mat.GetTexture("_Albedo");
        colorDefault = mat.GetColor("_Tint");
    }

    public void ChangeColor(string newColor) {
        //Coloring the object
        if (type == Type.Cube || type == Type.Wedge) {
            ColorCube(newColor);
            return;
        }
        else {
            ColorProp(newColor);
        }

        //Starting the animation on the object, if there is any
        if (type == Type.AnimatedProp) {
            animateArt.DetermineArtType(newColor);
        }
        else if (type == Type.SeaCreature) {
            if (newColor != "") {
                seaCreature.StartOrbit();
            }
            else {
                seaCreature.StopOrbit();
            }
        }
        
    }

    /// <summary>
    /// Changes which child mesh is active in hierarchy. Is used for coloring cubes and wedges.
    /// </summary>
    private void ColorCube(string newColor) {
        //Switching mesh
        ConvertColorToMesh(currentColor).SetActive(false);
        ConvertColorToMesh(newColor).SetActive(true);
        
        //Updating collider
        if (newColor == "Red")
            col.isTrigger = true;
        else
            col.isTrigger = false;

        //Setting tag
        if (gameObject.tag != "Untagged") {
            if (newColor == "")
                gameObject.tag = cubeType;
            else
                gameObject.tag = newColor;
        }

        //Saving the new color
        currentColor = newColor;
    }

    /// <summary>
    /// Changes properties in the material. Is used for coloring art props.
    /// </summary>
    private void ColorProp(string newColor) {
        //Setting color
        mat.SetColor("_Tint", ConvertStringToColor(newColor));

        //Setting albedo texture
        /*if (newColor == "") {
            mat.SetTexture("_Albedo", textureDefault);
        }
        else {
            mat.SetTexture("_Albedo", null);
        }*/
    }

    private Color ConvertStringToColor(string color) {
        switch (color) {
            case "":
                return colorDefault;
            case "Red":
                return new Color(0.6f, 0f, 0f);
            case "Green":
                return new Color(0f, 0.6f, 0f);
            case "Blue":
                return new Color(0f, 0f, 0.6f);
            default:
                return Color.magenta;
        }
    }

    private GameObject ConvertColorToMesh(string color) {
        switch (color) {
            case "Red":
                return red;
            case "Green":
                return green;
            case "Blue":
                return blue;
            case "":
                return noColor;
            default:
                return noColor;
        }
    }

    /*
    [Tooltip("Is this is a Cube or a Wedge?")]
    public string cubeType;
    private bool animIsPlaying = false;
    private Animator anim;

    [Tooltip("If left blank, the cube will just grab its texture at scene start.")]
    [SerializeField] Texture originalTexture;
    [Tooltip("If left blank, the cube will just grab its color at scene start.")]
    [SerializeField] Color originalColor = Color.magenta;
    private Material mat;
    private Collider col;

    private void Awake() {
        col = GetComponent<Collider>();
        //mat = GetComponentInChildren<MeshRenderer>().material ? GetComponentInChildren<MeshRenderer>().material : GetComponentInChildren<SkinnedMeshRenderer>().material;

        if (GetComponentInChildren<SkinnedMeshRenderer>() != null) {
            mat = GetComponentInChildren<SkinnedMeshRenderer>().material;
            anim = GetComponent<Animator>();
            anim.speed = 0;
        }
        else {
            mat = GetComponentInChildren<MeshRenderer>().material;
        }
        
        //originalTexture = mat.mainTexture;
        if(originalTexture == null)
            originalTexture = mat.GetTexture("_Albedo");
        if (originalColor == Color.magenta)
            originalColor = mat.GetColor("_Tint");

    }

    public void ChangeColor(string newColor) {
        if(GetComponentInChildren<SkinnedMeshRenderer>() != null) {
            whaleFlying flyingWhale = GetComponent<whaleFlying>();

            //Setting the new color
            mat.color = ConvertStringToColor(newColor);
            mat.SetColor("_Tint", ConvertStringToColor(newColor));

            if (!animIsPlaying && newColor != "") {
                flyingWhale.StartOrbit();
                anim.speed = 1;
                animIsPlaying = true;
                
            }
            if (newColor == "") {
                flyingWhale.StopOrbit();
                anim.speed = 0;
                animIsPlaying = false;
            }

            //Setting the new tag, texture and smoothness
            if (newColor == "") {
                if (gameObject.tag != "Untagged") {
                    gameObject.tag = cubeType;
                }
                mat.mainTexture = originalTexture;
                mat.SetFloat("_Glossiness", 0f);

                //mat.SetTexture("_Albedo", originalTexture);
            }
            else {
                if (gameObject.tag != "Untagged") {
                    gameObject.tag = newColor;
                }
                mat.mainTexture = null;
                mat.SetFloat("_Glossiness", 0.4f);

               // mat.SetTexture("_Albedo", null);
            }
        }
        else {
            AnimateArt animArt = GetComponent<AnimateArt>();

            if (animArt != null)
                animArt.DetermineArtType(newColor);

            //Setting the new color
            mat.color = ConvertStringToColor(newColor);
            mat.SetColor("_Tint", ConvertStringToColor(newColor));

            //Setting the new tag, texture and smoothness
            if (newColor == "") {
                if (gameObject.tag != "Untagged") {
                    gameObject.tag = cubeType;
                }
                mat.mainTexture = originalTexture;
                mat.SetFloat("_Glossiness", 0f);

                mat.SetTexture("_Albedo", originalTexture);
            }
            else {
                if (gameObject.tag != "Untagged") {
                    gameObject.tag = newColor;
                }
                mat.mainTexture = null;
                mat.SetFloat("_Glossiness", 0.4f);

                mat.SetTexture("_Albedo", null);
            }

            //Modifying the shader type and the collider in case the cube is colored red
            if (newColor == "Red") {
                col.isTrigger = true;
                mat.SetFloat("_Alpha", 0.4f);
            }
            else {
                col.isTrigger = false;
                mat.SetFloat("_Alpha", 1f);
            }
        }
    }

    private Color ConvertStringToColor(string color) {
        switch (color) {
            case "":
                return originalColor;
            case "Red":
                return new Color(0.6f, 0f, 0f);
            case "Green":
                return new Color(0f, 0.6f, 0f);
            case "Blue":
                return new Color(0f, 0f, 0.6f);
            default:
                return Color.magenta;
        }
    }*/
}
