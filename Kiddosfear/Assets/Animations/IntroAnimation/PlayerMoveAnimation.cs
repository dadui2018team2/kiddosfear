﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveAnimation : MonoBehaviour {

	public Animator animator;

	public void Run(){
		animator.SetBool("Run", true);
	}

	public void Stop(){
		animator.SetBool("Run", false);
		animator.SetBool("Falling", false);
	}

	public void Fall(){
		animator.SetBool("Falling", true);
	}

	public void Land(){
		animator.SetBool("Falling", false);
		animator.SetTrigger("Land");
	}
}
