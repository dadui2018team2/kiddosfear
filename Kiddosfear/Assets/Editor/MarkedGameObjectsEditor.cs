﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MarkedGameObjects))]
public class MarkedGameObjectsEditor : Editor {

	public string prefabLoadPath = "Assets/Prefabs/Environment";

	public override void OnInspectorGUI() {

		MarkedGameObjects mgo = (MarkedGameObjects)target;

		// Replace GameObjects -------------------------

		EditorGUILayout.LabelField("Replace objects with prefabs.", EditorStyles.boldLabel);
		prefabLoadPath = EditorGUILayout.TextField("Prefab load path", prefabLoadPath);
		if(GUILayout.Button("Refresh prefab list")){
			mgo.UpdatePrefabs(prefabLoadPath);
		}
		EditorGUILayout.Space();
		
		string[] prefabOptions = new string[mgo.prefabs.Length];
		for (int i = 0; i < prefabOptions.Length; i++) {
			prefabOptions[i] = mgo.prefabs[i].name;
		}
		mgo.prefabIndex = EditorGUILayout.Popup("Prefab selection", mgo.prefabIndex, prefabOptions);
		EditorGUILayout.Space();

		if (GUILayout.Button("Replace GameObjects")){
			mgo.ReplaceObjects();
		}
		EditorGUILayout.Space();


	}
} 
