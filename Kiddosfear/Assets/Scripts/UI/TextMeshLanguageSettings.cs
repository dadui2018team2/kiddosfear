﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextMeshLanguageSettings : MonoBehaviour {

    [SerializeField] TextMesh textMesh;

    [Tooltip("\\n for a new line. ALways test how your text looks ingame!")]
    public string english;
    [Tooltip("\\n for a new line. ALways test how your text looks ingame!")]
    public string danish;

    private string langKey = "lang";
    private int currentLang;

    private void Awake() {
        danish = danish.Replace("\\n", "\n");
        english = english.Replace("\\n", "\n");
    }

    public void LanguageObjectsUpdate() {
        TextMeshLanguageSettings[] lsArray = FindObjectsOfType<TextMeshLanguageSettings>();
        for (int i = 0; i < lsArray.Length; i++) {
            lsArray[i].UpdateThis();
        }
    }

    private void OnEnable() {
        UpdateThis();
    }

    private void UpdateThis() {
        currentLang = PlayerPrefs.GetInt(langKey, 0);
        if (textMesh != null) {
            if (currentLang == 0) {
                textMesh.text = english;
            }
            else {
                textMesh.text = danish;
            }
        }
    }
}
