﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[ExecuteInEditMode]
public class GenerateBox : MonoBehaviour {
#if UNITY_EDITOR
    /*
	This is for generating a "box" of x * y * z size.
	*/

    public GameObject[] prefabs;
	public int prefabIndex;
	public Vector3Int size;
	public float spacing;
	public Vector3 position;
	public bool shell;
	
	///<summary>Used to update the list of prefabs</summary>
	public void UpdatePrefabs(string path){
		string[] assetPaths = AssetDatabase.GetAllAssetPaths();
		
		List<GameObject> prefabList = new List<GameObject>();
		 for (int i = 0; i < assetPaths.Length; i++){
			 if (assetPaths[i].Contains(path)){
				GameObject _asset = (GameObject)AssetDatabase.LoadAssetAtPath(assetPaths[i], typeof(GameObject));
				try {
					string name = _asset.name;
					prefabList.Add(_asset);
				}
				catch (NullReferenceException e){
				}
			 }
		 }
		Debug.Log("Found " + assetPaths.Length + " assets, of which " + prefabList.Count + " are prefab(s).");
		prefabs = prefabList.ToArray();
	}

	///<summary>Generates a box</summary>
	public void Generate() {
		try {
			List<GameObject> newObjects = new List<GameObject>();
			for (int x = 0; x < size.x; x++){
				for (int y = 0; y < size.y; y++){
					for (int z = 0; z < size.z; z++){
						if (shell){
							if (x == 0 || x == size.x -1 || y == 0 || y == size.y - 1 || z == 0 || z == size.z - 1){
								newObjects.Add(Instantiate(prefabs[prefabIndex], (new Vector3(x, y, z) * spacing) + position, Quaternion.identity));
							}
						}
						else {
							newObjects.Add(Instantiate(prefabs[prefabIndex], (new Vector3(x, y, z) * spacing) + position, Quaternion.identity));
						}
					}
				}
			}
			Selection.objects = newObjects.ToArray();
		}
		
		catch (NullReferenceException e){
			Debug.Log("Null reference exception!");
		}
	}
#endif
}
