﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogViewer : MonoBehaviour {

    public int maxMessages;

    [SerializeField] private GameObject log;
    [SerializeField] private Text logText;

    private List<string> messages = new List<string>();

    public void ToggleLog() {
        if (log.activeInHierarchy) {
            log.SetActive(false);
        }
        else {
            log.SetActive(true);
        }
    }

    void OnEnable() {
        Application.logMessageReceived += LogMessage;
    }

    void OnDisable() {
        Application.logMessageReceived -= LogMessage;
    }

    private void LogMessage(string message, string stackTrace, LogType type) {
        if (messages.Count > maxMessages) {
            messages.RemoveAt(0);
        }

        string msg = "";
        msg += "<color=grey>" + stackTrace.Replace("\n", "") + ": </color>";

        if (type == LogType.Error || type == LogType.Exception) {
            msg += "<color=red>";
        }
        else {
            msg += "<color=white>";
        }

        msg += message.Replace("\n", " ") + "</color>\n";
        msg += "\n";

        messages.Add(msg);

        UpdateLogText();
    }

    private void UpdateLogText() {
        logText.text = "";
        foreach (string s in messages)
            logText.text += s;
    }
}
