﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RendererSortOrder : MonoBehaviour {

	public int sortingOrder = 0;

	// Use this for initialization
	void Start () {
		GetComponent<Renderer>().sortingOrder = sortingOrder;
	}
}
