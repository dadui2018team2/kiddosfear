﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeSystem : MonoBehaviour {

    CubeInteraction cubeInteraction;

    [Header("Player Attributes")]
    [Tooltip("This value is the amount of health the player starts with. Changes when the player loses/gains hp")]
    public int hitPoints;
    public int maxHitPoints;

    [Header("Player invulnerability variables")]
    public float invulnerabilityTimer;
    private bool canLoseHealth;

    [Header("Pickup variables")]
    public int pickupCounter;
    [Tooltip("The threshold for when the player gains a health after picking up a pickup")]
    public int pickupHealthGainThreshold;
    [Tooltip("Reference to the UI manager that controls life and pickups")]
    public ItemPickupUIManager pickupManager;

    private ActionAnimator actionAnimator;
    private Coroutine waitTimer;

    private PlayerBlink playerBlink;
    private PlayerMovement playerMovement;

    private string causeOfDeath;

    private void Awake() {
        cubeInteraction = GetComponent<CubeInteraction>();
        actionAnimator = FindObjectOfType<ActionAnimator>();
        canLoseHealth = true;
        playerBlink = GetComponent<PlayerBlink>();
        playerMovement = GetComponent<PlayerMovement>();
    }

    private void OnTriggerEnter(Collider other) {
        if (cubeInteraction.lastKnownGroundCube != null) {
            if (other.gameObject.tag == "KillZone") {
                causeOfDeath = other.gameObject.tag;
                LoseHealth();

                if(waitTimer == null) //i.e. if Kiddo did not die from falling
                    Respawn();
            }
        }
        if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Boss") {
            EnemyBehaviour enemy = other.GetComponent<EnemyBehaviour>();
            if (enemy != null) {
                enemy.EnemyHit();
            }
            Vector3 contactNormal = transform.position - other.transform.position;
            contactNormal.Normalize();
            contactNormal.y = 0;
            print(contactNormal);
            //transform.position = transform.position + (3 * contactNormal);
            playerMovement.EnemyPushback(transform.position - other.transform.position);
            if (canLoseHealth){
                causeOfDeath = other.gameObject.tag;
                LoseHealth();
                actionAnimator.TriggerHitAnim();
            }
        }
        if(other.gameObject.tag == "Pickup") {
            pickupCounter++;
            pickupManager.UpdatePickup(pickupCounter);
            PlayerPrefs.SetInt("PickUpUnlock", pickupCounter);
            Debug.Log("PlayerPrefs are now set to:  " + PlayerPrefs.GetInt("PickUpUnlock") + "and the counter is : " + pickupCounter);
            other.gameObject.SetActive(false);
            AudioPlayer.PlayEvent("PickUpLight", gameObject, 3f);
            if(pickupCounter >= pickupHealthGainThreshold) {
                AddHealth();
            }
        }
    }

    

    /// <summary>
    /// removes a health point and activates invulnerability
    /// </summary>
    private void LoseHealth() {
        hitPoints -= 1;
        pickupManager.RemoveLife(hitPoints);
        CheckDeath();
        StartCoroutine(StartInvulnerabilityTimer());
    }

    /// <summary>
    /// Add a health point if the player doens't have max hp
    /// </summary>
    public void AddHealth() {
        if (hitPoints < 3) {
            hitPoints++;
            pickupManager.AddLife(hitPoints);
        }
    }

    /// <summary>
    /// Set invulnerability on the player
    /// </summary>
    /// <returns></returns>
    private IEnumerator StartInvulnerabilityTimer() {
        canLoseHealth = false;
        playerBlink.StartBlink(invulnerabilityTimer);
        yield return new WaitForSeconds(invulnerabilityTimer);
        canLoseHealth = true;
    }

    /// <summary>
    /// If the players health reaches zero re-loads the scene
    /// </summary>
    private void CheckDeath() {
        if (hitPoints <= 0) {
            if (waitTimer == null) {
                waitTimer = StartCoroutine(PerformDeath());
            }
        }
    }

    /// <summary>
    /// Starts the death-animation and waits for it to finish before reloading the scene.
    /// </summary>
    private IEnumerator PerformDeath() {
        actionAnimator.TriggerDeathAnim();
        playerBlink.StopBlink();

        switch (causeOfDeath) {
            case "KillZone":
                AudioPlayer.PlayEvent("KiddoDieByFaaling", gameObject, 5f);
                FindObjectOfType<CameraController>().shouldFollow = false;
                break;
            case "Enemy":
            case "Boss":
                AudioPlayer.PlayEvent("KiddoDie", gameObject, 5f);
                GetComponent<PlayerMovement>().enabled = false;
                //GetComponent<Shooting>().enabled = false;
                FindObjectOfType<TouchPaint>().gameObject.SetActive(false);
                break;
            default:
                Debug.LogError("No death-sound to play for cause of death: " + causeOfDeath);
                break;
        }

        while (actionAnimator.HasFinishedAnimation("death") == false)
            yield return new WaitForEndOfFrame();

        FindObjectOfType<CameraController>().shouldFollow = true;
        waitTimer = null;
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Moves the player back to the last known cube.
    /// </summary>
    private void Respawn() {
        GetComponent<ReturnPlayerToGround>().ReturnPlayer();
    }
}
