﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Takes a Linerenderer into the inspector both as LineRenderer and Gameobject(for disabling when not playing)

public class ShotChargeUp : MonoBehaviour
{
    private float strenght = 0f;
    private float strenghtTwo = 0f;
    private float strenghtThree = 0f;
    private float strenghtFour = 0f;
    public LineRenderer lrOne;
    public LineRenderer lrTwo;
    public LineRenderer lrThree;
    public LineRenderer lrFour;
    public LineRenderer olr;
    private bool charge = false;
    public GameObject LineRend;
    public Shooting shooting;


    [Header("Lenght of Colours")]
    public float LineOneLenght = 2f;
    public float LineTwoLenght = 4f;
    public float LineThreeLenght = 6f;
    public float LineFourLenght = 8.5f;

    [Header("Charge Up Speed(Visual only)")]
    public float ChargeSpeed = 3f;

    public void ChargeShootYES()
    {
        charge = true;
        LineRend.SetActive(true);
    }

    public void ChargeShootNO()
    {
        charge = false;
        strenght = 0;
        strenghtTwo = 0;
        strenghtThree = 0;
        strenghtFour = 0;

        Vector3[] lrOne_positions = new Vector3[2];
        lrOne_positions[0] = new Vector3(0f, 0f, 0f);
        lrOne_positions[1] = new Vector3(0f, 0f, 0f);
        lrOne.positionCount = lrOne_positions.Length;
        lrOne.SetPositions(lrOne_positions);

        Vector3[] lrTwo_positions = new Vector3[2];
        lrTwo_positions[0] = new Vector3(0f, 0f, 0f);
        lrTwo_positions[1] = new Vector3(0f, 0f, 0f);
        lrTwo.positionCount = lrTwo_positions.Length;
        lrTwo.SetPositions(lrTwo_positions);

        Vector3[] lrThree_positions = new Vector3[2];
        lrThree_positions[0] = new Vector3(0f, 0f, 0f);
        lrThree_positions[1] = new Vector3(0f, 0f, 0f);
        lrThree.positionCount = lrThree_positions.Length;
        lrThree.SetPositions(lrThree_positions);

        Vector3[] lrFour_positions = new Vector3[2];
        lrFour_positions[0] = new Vector3(0f, 0f, 0f);
        lrFour_positions[1] = new Vector3(0f, 0f, 0f);
        lrFour.positionCount = lrFour_positions.Length;
        lrFour.SetPositions(lrFour_positions);

        Vector3[] opositions = new Vector3[2];
        opositions[0] = new Vector3(0f, 0f, 0f);
        opositions[1] = new Vector3(0f, 0f, 0f);
        olr.positionCount = opositions.Length;
        olr.SetPositions(opositions);
        LineRend.SetActive(false);
    }
    void Start()
    {
        
    }
    void Update()
    {
        
        if (charge == true)
        {

           
            strenght += (ChargeSpeed * Time.deltaTime);
            strenghtTwo += (ChargeSpeed * Time.deltaTime);
            strenghtThree += (ChargeSpeed * Time.deltaTime);
            strenghtFour += (ChargeSpeed * Time.deltaTime);
            Vector3[] lrOne_positions = new Vector3[2];
            lrOne_positions[0] = GameManager.Player.transform.position;
            lrOne_positions[1] = shooting.shootDirection * strenght + GameManager.Player.transform.position;
            lrOne.positionCount = lrOne_positions.Length;
            lrOne.SetPositions(lrOne_positions);

            Vector3[] opositions = new Vector3[2];
            opositions[0] = GameManager.Player.transform.position;
            opositions[1] = shooting.shootDirection * (LineFourLenght-0.5f) + GameManager.Player.transform.position;
            olr.positionCount = opositions.Length;
            olr.SetPositions(opositions);

            //Checks that the values for each segments end position doesnt 
            if (strenghtFour >= LineFourLenght)
            {
                strenght = 0;
                strenghtTwo = 0;
                strenghtThree = 0;
                strenghtFour = 0;
                charge = false;
                LineRend.SetActive(false);
            }
            if (strenght >= LineOneLenght)
            {
                strenght = LineOneLenght;
                Vector3[] lrTwo_positions = new Vector3[2];
                lrTwo_positions[0] = shooting.shootDirection * strenght + GameManager.Player.transform.position;
                lrTwo_positions[1] = shooting.shootDirection * strenghtTwo + GameManager.Player.transform.position;
                lrTwo.positionCount = lrTwo_positions.Length;
                lrTwo.SetPositions(lrTwo_positions);
            }
            if (strenghtTwo >= LineTwoLenght)
            {
                strenghtTwo = LineTwoLenght;
                Vector3[] lrThree_positions = new Vector3[2];
                lrThree_positions[0] = shooting.shootDirection * strenghtTwo + GameManager.Player.transform.position;
                lrThree_positions[1] = shooting.shootDirection * strenghtThree + GameManager.Player.transform.position;
                lrThree.positionCount = lrThree_positions.Length;
                lrThree.SetPositions(lrThree_positions);
            }
            if (strenghtThree >= LineThreeLenght)
            {
                strenghtThree = LineThreeLenght;
                Vector3[] lrFour_positions = new Vector3[2];
                lrFour_positions[0] = shooting.shootDirection * strenghtThree + GameManager.Player.transform.position;
                lrFour_positions[1] = shooting.shootDirection * strenghtFour + GameManager.Player.transform.position;
                lrFour.positionCount = lrFour_positions.Length;
                lrFour.SetPositions(lrFour_positions);
            }
        }


    }
}
